package com.tatsuya.service;


import com.tatsuya.dao.PVPDAO;
import com.tatsuya.entity.PvPrediction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PVPServiceImpl implements IPVPService {

    @Autowired
    private PVPDAO pvpDAO;

    @Override
    public List<PvPrediction> findPVP(String domain, String date, String category) {
        return this.pvpDAO.findPVP(domain, date, category);
    }

    @Override
    public List<PvPrediction> getAllPVP() {
        return this.pvpDAO.getAllPVP();
    }
}
