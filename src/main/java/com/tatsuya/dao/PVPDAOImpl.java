package com.tatsuya.dao;

import com.tatsuya.entity.PvPrediction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Transactional
@Repository
public class PVPDAOImpl implements PVPDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<PvPrediction> findPVP(String domain, String date, String category) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        int month = -1;
        int dayOfMonth = -1;
        try {

            Date startDate = df.parse(date);
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
            month = cal.get(Calendar.MONTH) + 1;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String tableName = "PvPrediction";
        String hql = "SELECT p FROM " + tableName + " p where p.domain = :domain and p.category = :category and p.month = :month and p.day = :day";

        Query query = entityManager
                .createQuery(hql)
                .setParameter("domain", domain)
                .setParameter("category", category)
                .setParameter("month", month)
                .setParameter("day", dayOfMonth);
        return query.getResultList();

    }

    @Override
    public List<PvPrediction> getAllPVP() {
        CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(PvPrediction.class));
        return entityManager.createQuery(cq).getResultList();
    }

}
