package com.tatsuya.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "pvprediction")

public class PvPrediction implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "month")
    private int month;

    @Id
    @Column(name = "day")
    private int day;

    @Id
    @Column(name = "dow")
    private int dayOfWeek;

    @Id
    @Column(name = "hour")
    private int hour;

    @Id
    @Column(name = "category")
    private String category;

    @Id
    @Column(name = "domain")
    private String domain;

    @Column(name = "predictionresult")
    private String predictionResult;

    @Column(name = "actualresult")
    private String actualResult;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getPredictionResult() {
        return predictionResult;
    }

    public void setPredictionResult(String predictionResult) {
        this.predictionResult = predictionResult;
    }

    public String getActualResult() {
        return actualResult;
    }

    public void setActualResult(String actualResult) {
        this.actualResult = actualResult;
    }
}