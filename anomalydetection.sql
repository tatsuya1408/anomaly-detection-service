-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: anomalydetection
-- ------------------------------------------------------
-- Server version	5.7.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pvprediction`
--

DROP TABLE IF EXISTS `pvprediction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pvprediction` (
  `month` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `dow` int(11) NOT NULL,
  `hour` int(11) NOT NULL,
  `category` varchar(45) NOT NULL,
  `domain` varchar(100) NOT NULL,
  `predictionresult` double DEFAULT NULL,
  `actualresult` double DEFAULT NULL,
  PRIMARY KEY (`month`,`day`,`dow`,`hour`,`category`,`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pvprediction`
--

LOCK TABLES `pvprediction` WRITE;
/*!40000 ALTER TABLE `pvprediction` DISABLE KEYS */;
INSERT INTO `pvprediction` VALUES (8,30,5,0,'home','dantri.com.vn',105210.405157138,-1),(8,30,5,0,'kinh-doanh','dantri.com.vn',43735.7424159774,-1),(8,30,5,0,'phap-luat','dantri.com.vn',44440.5112451029,-1),(8,30,5,0,'the-gioi','dantri.com.vn',48488.0754311285,-1),(8,30,5,0,'the-thao','dantri.com.vn',59874.3530094454,-1),(8,30,5,0,'xa-hoi','dantri.com.vn',56105.0848626143,-1),(8,30,5,1,'home','dantri.com.vn',105459.846303357,-1),(8,30,5,1,'kinh-doanh','dantri.com.vn',43985.1835621963,-1),(8,30,5,1,'phap-luat','dantri.com.vn',44689.9523913218,-1),(8,30,5,1,'the-gioi','dantri.com.vn',48737.5165773474,-1),(8,30,5,1,'the-thao','dantri.com.vn',60123.7941556643,-1),(8,30,5,1,'xa-hoi','dantri.com.vn',56354.5260088332,-1),(8,30,5,2,'home','dantri.com.vn',105709.287449576,-1),(8,30,5,2,'kinh-doanh','dantri.com.vn',44234.6247084152,-1),(8,30,5,2,'phap-luat','dantri.com.vn',44939.3935375407,-1),(8,30,5,2,'the-gioi','dantri.com.vn',48986.9577235663,-1),(8,30,5,2,'the-thao','dantri.com.vn',60373.2353018832,-1),(8,30,5,2,'xa-hoi','dantri.com.vn',56603.9671550521,-1),(8,30,5,3,'home','dantri.com.vn',105958.728595795,-1),(8,30,5,3,'kinh-doanh','dantri.com.vn',44484.0658546341,-1),(8,30,5,3,'phap-luat','dantri.com.vn',45188.8346837596,-1),(8,30,5,3,'the-gioi','dantri.com.vn',49236.3988697852,-1),(8,30,5,3,'the-thao','dantri.com.vn',60622.6764481021,-1),(8,30,5,3,'xa-hoi','dantri.com.vn',56853.408301271,-1),(8,30,5,4,'home','dantri.com.vn',106208.169742013,-1),(8,30,5,4,'kinh-doanh','dantri.com.vn',44733.507000853,-1),(8,30,5,4,'phap-luat','dantri.com.vn',45438.2758299785,-1),(8,30,5,4,'the-gioi','dantri.com.vn',49485.8400160041,-1),(8,30,5,4,'the-thao','dantri.com.vn',60872.117594321,-1),(8,30,5,4,'xa-hoi','dantri.com.vn',57102.8494474899,-1),(8,30,5,5,'home','dantri.com.vn',106457.610888232,-1),(8,30,5,5,'kinh-doanh','dantri.com.vn',44982.9481470719,-1),(8,30,5,5,'phap-luat','dantri.com.vn',45687.7169761974,-1),(8,30,5,5,'the-gioi','dantri.com.vn',49735.281162223,-1),(8,30,5,5,'the-thao','dantri.com.vn',61121.5587405399,-1),(8,30,5,5,'xa-hoi','dantri.com.vn',57352.2905937088,-1),(8,30,5,6,'home','dantri.com.vn',106707.052034451,-1),(8,30,5,6,'kinh-doanh','dantri.com.vn',45232.3892932908,-1),(8,30,5,6,'phap-luat','dantri.com.vn',45937.1581224163,-1),(8,30,5,6,'the-gioi','dantri.com.vn',49984.7223084419,-1),(8,30,5,6,'the-thao','dantri.com.vn',61370.9998867588,-1),(8,30,5,6,'xa-hoi','dantri.com.vn',57601.7317399277,-1),(8,30,5,7,'home','dantri.com.vn',106956.49318067,-1),(8,30,5,7,'kinh-doanh','dantri.com.vn',45481.8304395097,-1),(8,30,5,7,'phap-luat','dantri.com.vn',46186.5992686352,-1),(8,30,5,7,'the-gioi','dantri.com.vn',50234.1634546608,-1),(8,30,5,7,'the-thao','dantri.com.vn',61620.4410329777,-1),(8,30,5,7,'xa-hoi','dantri.com.vn',57851.1728861466,-1),(8,30,5,8,'home','dantri.com.vn',107205.934326889,-1),(8,30,5,8,'kinh-doanh','dantri.com.vn',45731.2715857286,-1),(8,30,5,8,'phap-luat','dantri.com.vn',46436.0404148541,-1),(8,30,5,8,'the-gioi','dantri.com.vn',50483.6046008797,-1),(8,30,5,8,'the-thao','dantri.com.vn',61869.8821791966,-1),(8,30,5,8,'xa-hoi','dantri.com.vn',58100.6140323655,-1),(8,30,5,9,'home','dantri.com.vn',107455.375473108,-1),(8,30,5,9,'kinh-doanh','dantri.com.vn',45980.7127319475,-1),(8,30,5,9,'phap-luat','dantri.com.vn',46685.481561073,-1),(8,30,5,9,'the-gioi','dantri.com.vn',50733.0457470986,-1),(8,30,5,9,'the-thao','dantri.com.vn',62119.3233254155,-1),(8,30,5,9,'xa-hoi','dantri.com.vn',58350.0551785844,-1),(8,30,5,10,'home','dantri.com.vn',107704.816619327,-1),(8,30,5,10,'kinh-doanh','dantri.com.vn',46230.1538781664,-1),(8,30,5,10,'phap-luat','dantri.com.vn',46934.9227072919,-1),(8,30,5,10,'the-gioi','dantri.com.vn',50982.4868933175,-1),(8,30,5,10,'the-thao','dantri.com.vn',62368.7644716344,-1),(8,30,5,10,'xa-hoi','dantri.com.vn',58599.4963248033,-1),(8,30,5,11,'home','dantri.com.vn',107954.257765546,-1),(8,30,5,11,'kinh-doanh','dantri.com.vn',46479.5950243853,-1),(8,30,5,11,'phap-luat','dantri.com.vn',47184.3638535108,-1),(8,30,5,11,'the-gioi','dantri.com.vn',51231.9280395364,-1),(8,30,5,11,'the-thao','dantri.com.vn',62618.2056178533,-1),(8,30,5,11,'xa-hoi','dantri.com.vn',58848.9374710222,-1),(8,30,5,12,'home','dantri.com.vn',108203.698911765,-1),(8,30,5,12,'kinh-doanh','dantri.com.vn',46729.0361706042,-1),(8,30,5,12,'phap-luat','dantri.com.vn',47433.8049997297,-1),(8,30,5,12,'the-gioi','dantri.com.vn',51481.3691857553,-1),(8,30,5,12,'the-thao','dantri.com.vn',62867.6467640722,-1),(8,30,5,12,'xa-hoi','dantri.com.vn',59098.3786172411,-1),(8,30,5,13,'home','dantri.com.vn',108453.140057984,-1),(8,30,5,13,'kinh-doanh','dantri.com.vn',46978.4773168231,-1),(8,30,5,13,'phap-luat','dantri.com.vn',47683.2461459486,-1),(8,30,5,13,'the-gioi','dantri.com.vn',51730.8103319742,-1),(8,30,5,13,'the-thao','dantri.com.vn',63117.0879102911,-1),(8,30,5,13,'xa-hoi','dantri.com.vn',59347.81976346,-1),(8,30,5,14,'home','dantri.com.vn',108702.581204202,-1),(8,30,5,14,'kinh-doanh','dantri.com.vn',47227.918463042,-1),(8,30,5,14,'phap-luat','dantri.com.vn',47932.6872921675,-1),(8,30,5,14,'the-gioi','dantri.com.vn',51980.2514781931,-1),(8,30,5,14,'the-thao','dantri.com.vn',63366.52905651,-1),(8,30,5,14,'xa-hoi','dantri.com.vn',59597.2609096789,-1),(8,30,5,15,'home','dantri.com.vn',108952.022350421,-1),(8,30,5,15,'kinh-doanh','dantri.com.vn',47477.3596092609,-1),(8,30,5,15,'phap-luat','dantri.com.vn',48182.1284383864,-1),(8,30,5,15,'the-gioi','dantri.com.vn',52229.692624412,-1),(8,30,5,15,'the-thao','dantri.com.vn',63615.9702027289,-1),(8,30,5,15,'xa-hoi','dantri.com.vn',59846.7020558978,-1),(8,30,5,16,'home','dantri.com.vn',109201.46349664,-1),(8,30,5,16,'kinh-doanh','dantri.com.vn',47726.8007554798,-1),(8,30,5,16,'phap-luat','dantri.com.vn',48431.5695846053,-1),(8,30,5,16,'the-gioi','dantri.com.vn',52479.1337706309,-1),(8,30,5,16,'the-thao','dantri.com.vn',63865.4113489478,-1),(8,30,5,16,'xa-hoi','dantri.com.vn',60096.1432021167,-1),(8,30,5,17,'home','dantri.com.vn',109450.904642859,-1),(8,30,5,17,'kinh-doanh','dantri.com.vn',47976.2419016987,-1),(8,30,5,17,'phap-luat','dantri.com.vn',48681.0107308242,-1),(8,30,5,17,'the-gioi','dantri.com.vn',52728.5749168498,-1),(8,30,5,17,'the-thao','dantri.com.vn',64114.8524951667,-1),(8,30,5,17,'xa-hoi','dantri.com.vn',60345.5843483356,-1),(8,30,5,18,'home','dantri.com.vn',109700.345789078,-1),(8,30,5,18,'kinh-doanh','dantri.com.vn',48225.6830479176,-1),(8,30,5,18,'phap-luat','dantri.com.vn',48930.4518770431,-1),(8,30,5,18,'the-gioi','dantri.com.vn',52978.0160630687,-1),(8,30,5,18,'the-thao','dantri.com.vn',64364.2936413856,-1),(8,30,5,18,'xa-hoi','dantri.com.vn',60595.0254945545,-1),(8,30,5,19,'home','dantri.com.vn',109949.786935297,-1),(8,30,5,19,'kinh-doanh','dantri.com.vn',48475.1241941365,-1),(8,30,5,19,'phap-luat','dantri.com.vn',49179.893023262,-1),(8,30,5,19,'the-gioi','dantri.com.vn',53227.4572092876,-1),(8,30,5,19,'the-thao','dantri.com.vn',64613.7347876045,-1),(8,30,5,19,'xa-hoi','dantri.com.vn',60844.4666407734,-1),(8,30,5,20,'home','dantri.com.vn',110199.228081516,-1),(8,30,5,20,'kinh-doanh','dantri.com.vn',48724.5653403554,-1),(8,30,5,20,'phap-luat','dantri.com.vn',49429.3341694809,-1),(8,30,5,20,'the-gioi','dantri.com.vn',53476.8983555065,-1),(8,30,5,20,'the-thao','dantri.com.vn',64863.1759338234,-1),(8,30,5,20,'xa-hoi','dantri.com.vn',61093.9077869923,-1),(8,30,5,21,'home','dantri.com.vn',110448.669227735,-1),(8,30,5,21,'kinh-doanh','dantri.com.vn',48974.0064865743,-1),(8,30,5,21,'phap-luat','dantri.com.vn',49678.7753156998,-1),(8,30,5,21,'the-gioi','dantri.com.vn',53726.3395017254,-1),(8,30,5,21,'the-thao','dantri.com.vn',65112.6170800423,-1),(8,30,5,21,'xa-hoi','dantri.com.vn',61343.3489332112,-1),(8,30,5,22,'home','dantri.com.vn',110698.110373954,-1),(8,30,5,22,'kinh-doanh','dantri.com.vn',49223.4476327932,-1),(8,30,5,22,'phap-luat','dantri.com.vn',49928.2164619187,-1),(8,30,5,22,'the-gioi','dantri.com.vn',53975.7806479443,-1),(8,30,5,22,'the-thao','dantri.com.vn',65362.0582262612,-1),(8,30,5,22,'xa-hoi','dantri.com.vn',61592.7900794301,-1),(8,30,5,23,'home','dantri.com.vn',110947.551520173,-1),(8,30,5,23,'kinh-doanh','dantri.com.vn',49472.8887790121,-1),(8,30,5,23,'phap-luat','dantri.com.vn',50177.6576081376,-1),(8,30,5,23,'the-gioi','dantri.com.vn',54225.2217941632,-1),(8,30,5,23,'the-thao','dantri.com.vn',65611.4993724801,-1),(8,30,5,23,'xa-hoi','dantri.com.vn',61842.231225649,-1);
/*!40000 ALTER TABLE `pvprediction` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-03 21:16:03
