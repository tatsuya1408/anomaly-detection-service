package com.tatsuya.controller;

import com.tatsuya.entity.PvPrediction;
import com.tatsuya.service.IPVPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import com.ecyrd.speed4j.StopWatch;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/pvp")
public class PVPController extends BaseService {

    @Autowired
    private IPVPService iPVPService;

    @GetMapping("article/{id}")
    public ResponseEntity<PvPrediction> getArticleById(
            @PathVariable("id") Integer id
    ) {
        PvPrediction pvPrediction = null;
        return new ResponseEntity<PvPrediction>(pvPrediction, HttpStatus.OK);
    }

    @GetMapping("/info/{domain}/{date}/{category}")
    public ResponseEntity<List<PvPrediction>> getAllArticles(
            @PathVariable String domain,
            @PathVariable String date,
            @PathVariable String category,
            HttpServletRequest request
    ) {
        StopWatch sw = new StopWatch();
        String requestUri = request.getRequestURI() + "?" + request.getQueryString();
        requestLogger.info("finish process request {}", requestUri);
        List<PvPrediction> list = iPVPService.getAllPVP();
        return new ResponseEntity<List<PvPrediction>>(list, HttpStatus.OK);
    }

    @GetMapping("/info")
    public ResponseEntity<List<PvPrediction>> getPVP(
            @RequestParam("domain") String domain,
            @RequestParam("date") String date,
            @RequestParam("category") String category,
            HttpServletRequest request
    ) {

        StopWatch sw = new StopWatch();
        String requestUri = request.getRequestURI() + "?" + request.getQueryString();
        requestLogger.info("finish process request {} in {}", requestUri, sw.stop());
        List<PvPrediction> list = this.iPVPService.findPVP(domain, date, category);
        return new ResponseEntity<List<PvPrediction>>(list, HttpStatus.OK);
    }

//    @PostMapping("article")
//    public ResponseEntity<Void> addArticle(@RequestBody PvPrediction pvPrediction, UriComponentsBuilder builder) {
//        boolean flag = iPVPService.addArticle(pvPrediction);
//        if (flag == false) {
//            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
//        }
//        HttpHeaders headers = new HttpHeaders();
//        headers.setLocation(builder.path("/pvPrediction/{id}").buildAndExpand(pvPrediction.getArticleId()).toUri());
//        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
//    }

    @PutMapping("article")
    public ResponseEntity<PvPrediction> updateArticle(
            @RequestBody PvPrediction pvPrediction
    ) {
//        iPVPService.updateArticle(pvPrediction);
        return new ResponseEntity<PvPrediction>(pvPrediction, HttpStatus.OK);
    }

    @DeleteMapping("article/{id}")
    public ResponseEntity<Void> deleteArticle(
            @PathVariable("id") Integer id
    ) {
//        iPVPService.deleteArticle(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
} 