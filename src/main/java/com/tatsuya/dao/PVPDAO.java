package com.tatsuya.dao;


import com.tatsuya.entity.PvPrediction;

import java.util.List;


public interface PVPDAO {
    List<PvPrediction> findPVP(String domain, String date, String category);
    List<PvPrediction> getAllPVP();
}
 