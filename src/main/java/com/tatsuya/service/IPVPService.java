package com.tatsuya.service;


import com.tatsuya.entity.PvPrediction;

import java.util.List;

public interface IPVPService {
     List<PvPrediction> findPVP(String domain, String date, String category);
     List<PvPrediction> getAllPVP();
}
