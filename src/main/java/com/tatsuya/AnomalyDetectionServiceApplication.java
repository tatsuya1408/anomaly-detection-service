package com.tatsuya;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import java.io.File;
import java.util.Properties;

@SpringBootApplication
public class AnomalyDetectionServiceApplication {
    public static void main(String[] args) {
        final Config configReader = ConfigFactory.parseFile(new File("conf.properties"));
        Properties prop = new Properties();
        prop.put("server.port", configReader.getString("server.port"));

        SpringApplicationBuilder applicationBuilder = new SpringApplicationBuilder()
                .sources(AnomalyDetectionServiceApplication.class)
                .properties(prop);
        applicationBuilder.run(args);
    }
}
